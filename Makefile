CFLAGS = -O3 -fomit-frame-pointer -Werror
CC = /usr/bin/gcc
LDLIBS = -lgmp -lm

INSTALL_ALL = install -Dvm755

OBJS := gexpr gmpver divide gcd gcdext multiply pi rsa

all : $(OBJS)
$(OBJS): % : %.c
	$(CC) $(CFLAGS) $< -o $@ $(LDLIBS)

.PHONY : clean
clean :
	$(RM) $(OBJS)

.PHONY : install
install :
	for i in $(filter-out gexpr,$(OBJS)); do \
		$(INSTALL_ALL) $$i $(DESTDIR)/bin/$(addprefix gmp_, $$i) ; \
	done
	$(INSTALL_ALL) gexpr $(DESTDIR)/bin/gexpr
	$(INSTALL_ALL) runbench $(DESTDIR)/bin/gmpbench
